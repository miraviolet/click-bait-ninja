<?php
/**
 * Customizer sections.
 *
 * @package Click Bait Ninja
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ninja_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'ninja_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'click-bait-ninja' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'ninja_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'click-bait-ninja' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'click-bait-ninja' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'ninja_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'click-bait-ninja' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'ninja_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'click-bait-ninja' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'ninja_customize_sections' );
