<?php
/**
 * Customizer panels.
 *
 * @package Click Bait Ninja
 */

/**
 * Add a custom panels to attach sections too.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ninja_customize_panels( $wp_customize ) {

	// Register a new panel.
	$wp_customize->add_panel(
		'site-options', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => esc_html__( 'Site Options', 'click-bait-ninja' ),
			'description'    => esc_html__( 'Other theme options.', 'click-bait-ninja' ),
		)
	);
}
add_action( 'customize_register', 'ninja_customize_panels' );
