<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Click Bait Ninja
 */

if ( ! is_active_sidebar( 'sidebar-1', 'sidebar-2' ) ) {
	return;
}

// if ( ! is_active_sidebar(  ) ) {
// 	return;
// }
?>

<aside class="secondary widget1 widget-area col-l-4" role="complementary" id="sticky">
	<?php 
		dynamic_sidebar( 'sidebar-1' );
	?>
</aside>

<aside class="secondary widget2 widget-area col-l-4" role="complementary">
	<?php 
		dynamic_sidebar( 'sidebar-2' ); 
	?>
</aside><!-- .secondary -->

