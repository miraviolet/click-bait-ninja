<?php
/**
 * Template part for displaying archive posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Click Bait Ninja
 */
?>

<article <?php post_class(); ?>>
		<div class="archive-post-featured-image">
			<?php
				if ( has_post_thumbnail() ) {
					echo '<a href="' . esc_url( get_permalink() ) . '" ';
					echo ');">';
					echo '<div class="thumbnail" href="' . esc_url( get_permalink() ) . '" ';
					echo 'style="background-image: url(';
					echo get_the_post_thumbnail_url();
					echo ');">';
					echo '</div>';
					echo '</a>';
				}
				else { 
					echo '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '<img src="' . get_bloginfo( 'stylesheet_directory' ) 
						. '/assets/images/thumbnail-default.png" />' . '</a>';
				}
				?>        
		</div>
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		if ( 'post' === get_post_type() ) :
		?>
		<div class="entry-meta">
			<?php
				if ( ! has_excerpt() ) {
					echo '';
			  } else { 
					the_excerpt();
			  }
				ninja_posted_on();
			 ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			// the_content( sprintf(
			// 	/* translators: %s: Name of current post. */
			// 	wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'click-bait-ninja' ), array(
			// 		'span' => array(
			// 			'class' => array(),
			// 		),
			// 	) ),
			// 	the_title( '<span class="screen-reader-text">"', '"</span>', false )
			// ) );

			// wp_link_pages( array(
			// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'click-bait-ninja' ),
			// 	'after'  => '</div>',
			// ) );
			
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php ninja_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->