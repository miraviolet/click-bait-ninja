<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Click Bait Ninja
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'click-bait-ninja' ); ?></h2>
</section>
