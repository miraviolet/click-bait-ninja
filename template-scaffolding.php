<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, ninja_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Click Bait Ninja
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'ninja_scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
